#!/bin/bash
#
# Based on instructions from this doc: https://docs.docker.com/engine/install/ubuntu/
#

echo "Run apt-get update..." && \
sudo apt-get update && \

echo "Install dependencies..." && \
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common && \

echo "Add Docker gpg key..." && \
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && \

echo "Check key fingerprint..." && \
sudo apt-key fingerprint 0EBFCD88 && \

echo "Add Docker apt repository..." && \
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" && \

echo "Installing Docker..." && \
sudo apt-get update && \
sudo apt-get install -y docker-ce docker-ce-cli containerd.io && \

echo "Testing install was sucessful..." && \
sudo docker run hello-world && \

echo "If you saw 'Hello from Docker', then the install was sucessful!" && \
echo "" && \

echo "Adding current user to Docker group..." && \
sudo usermod -aG docker "${USER}" && \

echo "Installing docker-compose..." && \
sudo curl -L "https://github.com/docker/compose/releases/download/1.28.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
sudo chmod +x /usr/local/bin/docker-compose && \

echo "Done!"
